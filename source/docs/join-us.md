---
title: Join Us!
date: 2020-11-20T04:46:56.724Z
updated: 2020-11-20T04:46:56.757Z
---
{% note primary %}
Please read all of this and [the rules](/docs/rules) if you want to join the site.
{% endnote %}

## Accessing the CMS

CMS stands for Content Management System, which is what you would use to write posts for a site. Think of it as a user-friendly interface like the one WordPress or Wattpad uses.

To access the CMS for this site, all you need to do is go to https://writing-buddies.netlify.app/admin. When you access this page, you should see a button saying "Login with Netlify Identity." Click that and you will be asked to log in or sign up.

### Signing Up

When you sign up for this site, you will be asked for your name, email, and password or you can sign up using Google, GitHub, GitLab, or BitBucket. Your name should be the name you want to be recognized by on the site, whether that is your real name or a pen name is up to you.

{% note warning %}
Be sure to remember your login info, if you lose it or forget it, I might not be able to recover them for you.
{% endnote %}

You will also need a username, that's just to identify you in posts. It will be the same name you used to sign up, however, it can't have spaces so replace all the spaces with dashes.

## Adding Content

After you have logged into the CMS, you should see a list of articles that have already been added to the site.

{% note danger %}
**DO NOT** touch content that does not belong to you **unless** you have been given permission by the original author. If you do this, you will be notified by the admin and receive a penalty. If you receive three penalties, you will be removed from the site.
{% endnote %}

On the left, you will see a list of folders. Each of these folders applies to the type of content and/or book they hold. The genre' folders, i.e. Mystery & Suspense, Romance, etc, contain single posts that fall under those categories. These tend to be really short stories with one page. The only exception is the "Articles" folder which is dedicated to blog posts, not any type of stories. Following those folders are the folders for specific books. Inside these folders are the chapters for that specific book. 

{% note info %}
At the current moment, you will not be able to create folders. So, **if you are adding a book with chapters,** read on.
{% endnote %}

To add posts to a specific folder, click on that folder and click on the button "New Post". Fill in the boxes on your left and save your work by clicking the "Save" button on the top left. On the right, you will see a basic preview of your post. 

Set the status of your post (Draft, In review, or Ready) by using the "Set status" dropdown menu to let the admins know if you're finished writing your post. When you're ready to publish the post, set the status of your post to "Ready" and an administrator will review your post.

{% note warning %}
**DO NOT** publish your post yet. The admins need to look at your post to make sure it does not go against any of the [rules](/docs/rules). Once they have approved it, they will publish your post for you.
{% endnote %}

{% note info %}
If there is anything wrong with your post, one of the admins will reach out to you about the issue.
{% endnote %}

Once your post is approved, it will show up on the site for everyone to see!

### Adding A Book
The first step is to complete the [Add A Book Form](/contact/#add-a-book). This will alert the admins and they will do the necessary steps on their end for your book to show up on the site. You will need to know the genre of your book, have a book cover (URL), and create a short description. If you look on the pages for each genre, that can give you an idea of what you will need and why.

{% note info %}
If you don't already have a book cover, you can look on sites like [Unsplash](https://unsplash.com) or [Pixabay](https://pixabay.com) for one. 

Don't forget to cite the author on your book's index page!
{% endnote %}

After you receive a confirmation from an admin, you should see a folder with the name of your book. Inside of it is a file called "index.md". This is the cover page of your book and should have the cover, complete book description, and author's name, edit it as you see fit. 

Inside your book title's folder is where you will put chapters for your book. There isn't a limit to have many chapters it can have, create a hundred if you want (why would you do that though?). Just, try not to combine books, that doesn't make sense.

---

That's how easy it is to add your own content! If you have any questions, leave a comment down below or [contact an administrator](/contact).