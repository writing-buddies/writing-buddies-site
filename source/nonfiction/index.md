---
title: Nonfiction
---

{% cq %}
  The challenge for a nonfiction writer is to achieve a poetic precision using the documents of truth but somehow to make people and places spring to life as if the reader was in their presence.
  [Simon Schama](https://www.brainyquote.com/authors/simon-schama-quotes)
{% endcq %}


{% linkgrid %}
My Editing Process | /nonfiction/my-editing-process/ | A quick summary of my editing process |
My Story Formula | /nonfiction/my-story-formula | How I create stories |
{% endlinkgrid %}