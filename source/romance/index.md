---
title: "Romance"
---
{% cq %}
  'Tis better to have loved and lost than never to have loved at all.
  [Alfred Lord Tennyson](https://www.brainyquote.com/authors/alfred-lord-tennyson-quotes)
{% endcq %}


{% linkgrid %}
Pur Ti Miro | /romance/pur-ti-miro/ | A violinist and a singer meet again after five years. Despite complications in their personal lives, will their delayed romance finally blend in a harmonious cadence? | https://www.brainyquote.com/photos_tr/en/a/alfredlordtennyson/153702/alfredlordtennyson1-2x.jpg
First Night to Ever After ~ a Brett x Reader one-shot | /romance/first-night-to-ever-after |  |
Dear Diary, Do You Wanna Know A Secret? | /romance/dear-diary-do-you-wanna-know-a-secret/ | Do you wanna know a secret? Because I've got a big one and it just may get me in a whole lot of trouble. If it doesn't kill me first. | https://example.com/example-image.jpg
{% endlinkgrid %}