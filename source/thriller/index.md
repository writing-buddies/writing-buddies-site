---
title: Thriller
---

{% cq %}
  Thrillers provide the reader with a safe escape into a dangerous world where the stakes are as high as can be imagined with unpredictable outcomes. It's a perfect genre in which to explore hard issues of good and evil, a mirror that allows the reader to see both the good and not so good in themselves.
  [Ted Dekker](https://www.brainyquote.com/authors/ted-dekker-quotes)
{% endcq %}


{% linkgrid %}
Kidnapped and Afraid | /thriller/kidnapped-and-afraid | He won't let her go, but he knows he can't keep her. She keeps escaping but he always finds her. He enjoys this game. And why is she starting to enjoy this too? | https://cdn.pixabay.com/photo/2019/05/06/11/24/portrait-4182841_960_720.jpg
{% endlinkgrid %}