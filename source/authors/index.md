---
title: Authors
---

# Bravisha Skietano
I'm the creator of this site and guild leader of Writing Buddies on Habitica

I'm an eighteen-year-old, homeschooled, Christian student who is struggling to stay motivated and finish things.
  
I love Jesus, pretty much any yarn crafts (crochet, knitting, macrame, been there done that), reading, writing, horses, developing websites (new at that), FOSS, and anything Linux (I'm not a complete tech geek but I definitely see myself getting there blush).

I'm very social, which isn't a very good thing since I trust people too easily wink, but I'm getting better.

I open my mouth a lot before thinking, but as I said, I'm getting better.

I procrastinate way too much but prayerfully I will learn some self-control and apply it everywhere in my life (starting with my mouth😋).

# Cherry
Hello! I'm a Capricorn Monkey, INFP-A, and an Enneagram Type Four.

I call myself a recovering workaholic, now advocating for self-love.

Self-betterment is a goal I try to pursue every day.

My life principles may be mostly ascribed to Buddhism, though I can't say for certain that I'm a practitioner.

I've been mostly vegetarian since the last week of October 2018, around the same time I signed up in Habitica.

I'm passionate about journalism and, more recently, rediscovered my fascination with classical music.

An old soul who still prefers writing with pen and paper.

# 8-Bally
High School Junior studying to become a future psychologist or Playwright.

Hobbies: Gaming, Theater, Writing, Music, Reading, Learning Psych

Zodiac Sign: cancer

MBTI: INFJ-T

Hogwarts House: snake

Avatar Element: fire

Bad Habits: YouTube Procrastination, Overdoing Electronics (add more when I think of some)

# Lidia Zakutnaya
