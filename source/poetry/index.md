---
title: Poetry
---

{% cq %}
  Poetry is language at its most distilled and most powerful.
  [Rita Dove](https://www.brainyquote.com/authors/rita-dove-quotes)
{% endcq %}


{% linkgrid %}
Today | /poetry/today | Sometimes I wonder what's different 'bout today, It doesn't take long till I find something to say... |
Prayer Is A Weapon | /poetry/prayer-is-a-weapon | Prayer is a weapon. You may not believe it but it's true. Prayer is the most powerful weapon you could ever choose... |
Why Did God Take My Daddy From Me? | /poetry/why-did-god-take-my-daddy-from-me | A young woman deals with the death of her beloved father and receives an unexpected gift. |
Happy Mother's Day | /poetry/happy-mothers-day | Happy Mother's Day to all the wonderful mothers in the world. Here is a poem for you. |
{% endlinkgrid %}