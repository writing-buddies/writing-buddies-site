---
authorURL: /#Bravisha_Skietano
sticky: 100
title: Welcome to Writing Buddies
date: 2020-11-14 12:41:02
updated: 2020-11-20T01:14:08.737Z
author: Yoda Elentiya Atolkien
username: Bravisha-Skietano
tags: null
---
Greetings! Thank you so much for visiting our site. In case you don't know what's going on here, let me explain. 

This is a communal blog for aspiring writers of all ages! Think of it as Wattpad or Inkitt but totally free and your content is *owned by you.* Of course, there are [some rules](/docs/rules) that you must follow and your content must be approved by the administrators; but, *overall, this site is for you.* So, feel free to write your heart out and not have to worry about who owns your content because *you do.*

Go ahead and check out our content! Click one of the genres above to read books or scroll down below for blog posts and flash fiction.