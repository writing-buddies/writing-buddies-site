---
title: Plays
---

{% cq %}
  All the world's a stage, and all the men and women merely players: they have their exits and their entrances; and one man in his time plays many parts, his acts being seven ages.
  [William Shakespeare](https://www.brainyquote.com/authors/william-shakespeare-quotes)
{% endcq %}


{% linkgrid %}
Checkmate | /plays/checkmate | A play written by 8-Bally |
{% endlinkgrid %}